package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {
    TextView tvNim1, tvNama1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        Intent it = getIntent();
        Bundle bd = it.getExtras();
        String nim = bd.getString("nim");
        String nama = bd.getString("nama");

        tvNim1 = findViewById(R.id.tvNim2);
        tvNama1 = findViewById(R.id.tvNama2);

        tvNim1.setText(nim);
        tvNama1.setText(nama);
    }
}