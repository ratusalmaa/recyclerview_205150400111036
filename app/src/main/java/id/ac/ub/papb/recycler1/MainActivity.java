package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    public static String TAG = "RV1";
    Button saveData;
    EditText nim;
    EditText nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        saveData = findViewById(R.id.bt1);
        nim = findViewById(R.id.etNim);
        nama = findViewById(R.id.editTextTextPersonName2);

        ArrayList<Mahasiswa> data = getData();
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
        saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nim1 = String.valueOf(nim.getText());
                String nama1 = String.valueOf(nama.getText());
                Mahasiswa mhs = new Mahasiswa();
                mhs.nim = nim1;
                mhs.nama = nama1;
                data.add(mhs);
                adapter.notifyDataSetChanged();
            }
        });
    }



    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG, "getData " + mhs.nim);
            data.add(mhs);
        }
        return data;
    }
}