package id.ac.ub.papb.recycler1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaAdapter.MahasiswaViewHolder> {
    LayoutInflater inflater;
    Context _context;
    ArrayList<Mahasiswa> data;

    public MahasiswaAdapter(Context _context, ArrayList<Mahasiswa> data) {
        this._context = _context;
        this.data = data;
        this.inflater = LayoutInflater.from(this._context);
    }

    @NonNull
    @Override
    public MahasiswaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MahasiswaViewHolder holder, int position) {
        Mahasiswa mhs = data.get(position);
        Log.d(MainActivity.TAG,"data "+position);
        Log.d(MainActivity.TAG,"nim "+mhs.nim);
        Log.d(MainActivity.TAG,"nama "+mhs.nama);

        holder.tvNim.setText(mhs.nim);
        holder.tvNama.setText(mhs.nama);
    }

    @Override
    public int getItemCount() {
        Log.d(MainActivity.TAG,"Jumlah data "+data.size());
        return data.size();
    }

    class MahasiswaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNim;
        TextView tvNama;

        public MahasiswaViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvNama = itemView.findViewById(R.id.tvNama);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        public void onClick(View view) {
            String nim = String.valueOf(tvNim.getText());
            String nama = String.valueOf(tvNama.getText());
            Intent it = new Intent(_context, Activity2.class);
            Bundle bd = new Bundle();
            bd.putString("nim", nim);
            bd.putString("nama", nama);
            it.putExtras(bd);
            _context.startActivity(it);
        }
    }
}